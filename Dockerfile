FROM php:8.1-fpm

LABEL maintainer="ashrul"
WORKDIR /var/www/php

### ARG SECTION ###
ARG POSTGRES_VERSION=12
ARG NODE_VERSION=lts
ARG WKHTML_VERSION=0.12.6.1-2
ARG WKHTML_HOST_VERSION=bullseye_amd64

### ENV SECTION ###
ENV TZ=Asia/Kuala_Lumpur
ENV PHPFPM__access.format '"%R - %u [%t] \"%m %r\" %s %l %Q %f"'

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && \
    apt-get install -y \
    # php php-fpm \
    wget \
    nano \
    gnupg2 \
    git \
    libzip-dev \
    libpng-dev \
    libjpeg-dev \
    zip \
    systemctl \
    libfreetype6-dev \
    supervisor \
    gosu \
    systemctl \
    nano \
    nginx \
    mariadb-server \
    net-tools 

RUN apt-get clean

# Allow remote connections
RUN sed -i 's/bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/' /etc/mysql/mariadb.conf.d/50-server.cnf

# Install required packages
RUN apt-get update && apt-get install -y apt-utils wget gnupg

## locales
RUN apt-get install -y \
    locales \
    && locale-gen \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen

## Node
RUN curl -fsSL https://deb.nodesource.com/setup_$NODE_VERSION.x | bash - \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && apt-get update && apt-get install -y nodejs

## PHP EXT mysql
RUN docker-php-ext-configure pdo_mysql --with-pdo-mysql=mysqlnd \
    && docker-php-ext-configure mysqli --with-mysqli=mysqlnd \
    && docker-php-ext-install pdo_mysql \
    && docker-php-ext-install mysqli

# PHP EXT File exif
RUN docker-php-ext-install exif && docker-php-ext-enable exif

# PHP EXT Zip
RUN docker-php-ext-install zip

# PHP EXT GD
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd

# PHP EXT Redis
RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis 

# PHP EXT pcntl
RUN docker-php-ext-configure pcntl --enable-pcntl \
    && docker-php-ext-install \
    pcntl

## Clean repo cache
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN apt-get -y autoremove
RUN apt-get clean

# service mariadb start
# CMD ["service", "mariadb", "start"]

# Grant privileges to root user for remote access
# RUN mysql -u root -e "GRANT ALL ON *.* TO 'root'@'%' IDENTIFIED BY 'P@ssw0rd' WITH GRANT OPTION; FLUSH PRIVILEGES;"

## Copy porject
COPY ./init.sh /usr/local/bin/docker-php-entrypoint
RUN chmod +x /usr/local/bin/docker-php-entrypoint


# RUN cp -r /var/www/statsdigital/setup/nginx/nginx.conf /etc/nginx/conf.d/default.conf
# ENTRYPOINT ["/var/www/php/init.sh"]